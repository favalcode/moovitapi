<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePublisherAdsUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publisher_ads_users', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('publisher_ads_id');
            // $table->foreignId('user_id')->constrained('users')->cascadeOnDelete();
            // $table->foreignId('publisher_ads_id')->constrained('publisher_ads')->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publisher_ads_users');
    }
}
