<?php

namespace App\Http\Controllers;

use App\Models\Graphic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GraphicController extends Controller
{
    public function createGraphic (Request $request) {
        try {
            $data = $request->all();
            $validator = Validator::make($data, [
                'name' => ['required', 'string', 'max:250'],
                'image' => ['required', 'image', 'mimes:png,jpg,svg,gif', 'max:2500']
            ]);

            if($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 401);
            };

            $path = $request->file('image')->store('public/storage/ads/images');

            $graphic = Graphic::create([
                'name' => $request->name,
                'image' => $path
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Graphic created successfully',
                'data' => $graphic
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th
            ], 503);
        }
    }
    
    public function getAll() {
        $graphic = Graphic::all();
        return response()->json([
            'success' => true,
            'data' => $graphic
            ], 200);
    }
}
