<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PublisherAdsUser extends Model
{
    use HasFactory;
    protected $table = 'publisher_ads_users';
}
